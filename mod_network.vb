﻿Imports System.Net
Imports System.Net.NetworkInformation
Imports System.Net.Sockets



Module mod_network

    'Public intTimeOut = 500

    Public Function Reverse_DNS_Lookup(ByVal strIp As String) As String

        Dim strResult As String = ""

        Try
            Dim host As IPHostEntry = Dns.GetHostByAddress(strIp)
            If (Trim(host.HostName) <> strIp) And host.HostName <> "" Then
                If host.HostName.Contains(".") Then
                    strResult = host.HostName

                End If
            End If


        Catch ex As Exception

        End Try

        Reverse_DNS_Lookup = strResult

    End Function

    Public Function isIPv4(ByVal ip As String) As Boolean
        If ip Is Nothing OrElse ip.Equals("") OrElse ip.Contains(":") OrElse ip.Contains(" ") Then
            Return False
        End If
        Dim m As System.Text.RegularExpressions.Match = System.Text.RegularExpressions.Regex.Match(ip, "\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b")
        If m.Success Then
            Return True
        End If
        Return False
    End Function

    Public Function isFQDN(ByVal fqdn As String) As Boolean
        If fqdn Is Nothing OrElse fqdn.Equals("") Or fqdn.Contains(":") Or fqdn.Contains(" ") Then
            Return False
        End If
        Dim m As System.Text.RegularExpressions.Match = System.Text.RegularExpressions.Regex.Match(fqdn, "(?=^.{1,254}$)(^(?:(?!\d+\.)[a-zA-Z0-9_\-]{1,63}\.?)+(?:[a-zA-Z]{2,})$)")
        If m.Success Then
            Return True
        End If
        Return False
    End Function

    Public Function CanConnectToTCPPort(ByVal ip As [String], ByVal port As Integer) As Boolean
        Dim sock As Socket = Nothing
        Try
            Dim timeout As Integer = intTimeout
            Dim ipe As New IPEndPoint(IPAddress.Parse(ip), port)
            sock = New Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp)

            Dim res As IAsyncResult = sock.BeginConnect(ipe, Nothing, Nothing)
            res.AsyncWaitHandle.WaitOne(intTimeout)
            If res.IsCompleted Then
                If sock.Connected Then
                    sock.Close()
                    Return True
                Else
                    Return False
                End If
            Else
                If sock.Connected Then
                    sock.Close()
                End If

            End If

        Catch e As SocketException
        Finally
            If sock IsNot Nothing OrElse sock.Connected Then
                sock.Close()

            End If
        End Try
        Return False
    End Function


    Public Function ResolveFQDN_to_IPv4(ByVal strFQDN As String) As String

        Dim strResult As String
        strResult = ""

        Try
            If strFQDN Is Nothing OrElse strFQDN.Equals("") Then
                strResult = ""
            Else
                If Right(strFQDN, 1) <> "." Then
                    strFQDN = strFQDN & "."
                End If

                Dim ips As IPAddress() = Dns.GetHostAddresses(strFQDN)
                For Each ip As IPAddress In ips
                    If isIPv4(ip.ToString()) = False Then
                        Continue For
                    Else
                        strResult = ip.ToString()
                        'Console.WriteLine(strResult)

                        Exit For
                    End If
                Next
            End If
        Catch e As Exception
            ' Console.WriteLine(e.Message);

        End Try

        ResolveFQDN_to_IPv4 = strResult



    End Function

End Module
