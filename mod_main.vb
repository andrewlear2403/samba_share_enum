﻿Imports System.Threading
Imports System.IO

Module mod_main

    Public Class FQDNResolvingThreadObj
        Public ObjectID As Integer
        Public signal As ManualResetEvent
        Public fqdn As [String]
        Public ip As [String]
    End Class

    Public dictIPs As New Dictionary(Of String, Integer)
    Public dictFQDNs As New Dictionary(Of String, String)
    Public dictIPsFromFile As New Dictionary(Of String, Integer)

    Dim dateStartTimeIPScan As DateTime
    Dim dateFinishTimeIPScan As DateTime

    Public intMaxThreads As Integer = 1
    Public intTimeout As Integer = 300

    Dim blnSingleHostToScan As Boolean = False
    Dim strIPToScan As String = ""
    Dim strFQDNToScan As String = ""

    Public dictIPs_to_FQDN As New Dictionary(Of String, String)

    Public PerformReverseLookups As Boolean = False

    Public threads2Lock As New [Object]()
    Public threadsStartedFQDN As Integer = 0

    Public threadsStartedSMBScan As Integer = 0

    Public strInputFile As String = ""

    Public strOutputFolder As String = ""
    Public strServer As String = ""
    Public blnShowAdminShares As Boolean

    Public Class SMBScanThreadObj
        Public ObjectID As Integer
        Public signal As ManualResetEvent
        Public ip As [String]
    End Class

    Function IsRunningInIDE() As String

        Dim p As Process = Process.GetCurrentProcess()
        Dim result As Boolean = False

        If p.ProcessName.ToLower().Trim().IndexOf("vshost") <> -1 Then
            result = True
        End If
        p.Dispose()

        IsRunningInIDE = result

    End Function

    Sub Main()

        PopulateMacAddressDict()

        dictIPs_to_FQDN.Clear() ' This will contain any FQDNs to IP that are resolved

        ProcessCommandLineSwitches()
        SetupEnv()

        SetNumberOfThreadsBasedUponProcessors()

        Dim intShareCount As Integer
        intShareCount = 0



        blnShowAdminShares = False

        Dim strShare As String = ""
        Dim strPathOnFS As String = ""
        Dim strCurrentConnections As String = ""
        Dim strShareComment As String = ""
        Dim strFullSharePath As String = ""

        Dim strScanDate As String = ""
        strScanDate = DateTime.Now.ToString("yyyyMMdd")


        If blnSingleHostToScan = True Then
            If strIPToScan <> "" Then
                Console.WriteLine("IP of " & strIPToScan & " will be scanned")
            End If
            If strFQDNToScan <> "" Then
                Console.WriteLine("FQDN of " & strFQDNToScan & " will be resolved to IPv4, then scanned")
            End If
        End If

        If blnSingleHostToScan = False Then

            ThreadPool.SetMaxThreads(intMaxThreads, intMaxThreads)
            Console.WriteLine("Maximum threads will be " & intMaxThreads)
            Console.WriteLine("Timeout for an unresponsive TCP port is " & intTimeout & "ms")

            If PerformReverseLookups = True Then
                Console.WriteLine("Reverse DNS lookups will be performed")
            End If

            
            If strInputFile <> "" Then
                If dictIPs.Count > 0 Then
                    If dictIPs.Count = 1 Then
                        Console.Write("Input file = '" & strInputFile & "' contains 1 unique IPv4 address ")
                    Else
                        Console.Write("Input file = '" & strInputFile & "' contains " & dictIPsFromFile.Count.ToString() & " unique IPv4 addresses ")
                    End If
                    'Console.Write("Input file = '" & strInputFile & "' contains " & dictIPsFromFile.Count.ToString() & " unique IPv4 addresses")
                    If dictFQDNs.Count > 0 Then
                        Console.Write("and " & dictFQDNs.Count.ToString() & " unique FQDNs")
                    End If
                    Console.Write(vbCrLf)
                Else
                    If dictFQDNs.Count > 0 Then
                        Console.WriteLine("Input file = '" & strInputFile & "' contains 0 IPv4 addresses ")
                    Else
                        Console.WriteLine("ERROR - Unable to detect any IPv4 or FQDNs in Input file '" & strInputFile & "'")
                        Console.WriteLine("Unable to process, so quitting")
                        Environment.Exit(98)
                    End If
                End If

            End If

        End If

        Dim Counter As Integer

        If dictFQDNs.Count > 0 And blnSingleHostToScan = False Then

            Dim intIPsBefore As Long
            intIPsBefore = dictIPs.Count

            Console.WriteLine(dictFQDNs.Count & " FQDNs will now also be resolved, and if not already in the stack will be added to IPv4 scan list")

            Dim totalThreadsFQDN As Long = 0


            Console.WriteLine("")
            Console.WriteLine(DateTime.Now.ToString() & " - Resolving FQDNs begins")
            Console.WriteLine("")
            ThreadPool.SetMaxThreads(intMaxThreads, intMaxThreads)

            Counter = 0
            For Each pair In dictFQDNs

                Dim obj As New FQDNResolvingThreadObj()
                obj.ObjectID = Counter
                obj.signal = New ManualResetEvent(False)
                obj.fqdn = pair.Key.ToString
                events.Add(obj.signal)
                Dim callback As New WaitCallback(AddressOf ResolveFQDNCaller)
                ThreadPool.QueueUserWorkItem(callback, obj)

                Counter += 1

            Next

            While Not HasFinished(events.ToArray())

                Dim perc As Double = (CDbl(GetNumberOfFQDNThreadsStarted()) / CDbl(dictFQDNs.Count))
                Dim percentage As Integer = CInt(perc * 100)
                WriteProgress(percentage)

                Thread.Sleep(400)
            End While

            WaitForAll(events.ToArray())
            WriteProgress(100)
            Console.WriteLine("")
            Console.WriteLine("")
            Console.WriteLine(DateTime.Now.ToString() & " - Resolving FQDNs finished")
            Console.WriteLine("")

            Dim intIPsAfter As Long
            intIPsAfter = dictIPs.Count

            If intIPsAfter > intIPsBefore Then
                Dim intIPDelta As Long
                intIPDelta = intIPsAfter - intIPsBefore
                Console.WriteLine(intIPDelta & " additional IPv4 addresses to scan by resolving FQDNs")
                If intIPDelta = dictFQDNs.Count Then
                    Console.WriteLine("Every FQDN resolved to a IPv4 address")
                End If
            End If

            
        Else
            If blnSingleHostToScan = True Then
                If strFQDNToScan <> "" And strIPToScan = "" Then
                    Console.WriteLine("Attempting to resolve " & strFQDNToScan)
                    strIPToScan = ResolveFQDN_to_IPv4(strFQDNToScan)
                    If strIPToScan <> "" Then
                        Console.WriteLine(strFQDNToScan & " resolves as " & strIPToScan)
                        dictIPs.Clear()
                        dictIPs.Add(strIPToScan, 0)
                        dictIPs_to_FQDN.Clear()
                        dictIPs_to_FQDN.Add(strIPToScan, strFQDNToScan)
                        
                    Else
                        Console.WriteLine(strFQDNToScan & " does not resolve in DNS")
                        Environment.Exit(1)
                    End If
                End If

            End If
        End If






        ' All work is now defined .. the stack of dictIPs contains everything we need

        If dictIPs.Count = 0 And strIPToScan = "" And strFQDNToScan = "" Then
            Console.WriteLine("Can't see any IPv4 addresses to scan .. quitting")
            Environment.Exit(101)
        End If

        If dictIPs.Count > 1 Then
            Console.WriteLine("A grand total of " & dictIPs.Count & " unique IPv4 addresses will be scanned")
            Console.WriteLine("")
        End If



      




        events.Clear()

        If dictIPs.Count > 1 Then
            dateStartTimeIPScan = DateTime.Now()
            Console.WriteLine("")
            Console.WriteLine(DateTime.Now.ToString() & " - Scanning of SMB Shares begins")
            Console.WriteLine("")

            ThreadPool.SetMaxThreads(intMaxThreads, intMaxThreads)

            Counter = 0
            For Each pair In dictIPs

                Dim obj As New SMBScanThreadObj()
                obj.ObjectID = Counter
                obj.signal = New ManualResetEvent(False)
                obj.ip = pair.Key.ToString
                events.Add(obj.signal)
                Dim callback As New WaitCallback(AddressOf SMBScanCaller)
                ThreadPool.QueueUserWorkItem(callback, obj)
                Counter += 1

            Next

            While Not HasFinished(events.ToArray())

                Dim perc As Double = (CDbl(GetNumberOfSMBScanThreadsStarted()) / CDbl(dictIPs.Count))
                Dim percentage As Integer = CInt(perc * 100)
                WriteProgress(percentage)

                Thread.Sleep(400)
            End While

            WaitForAll(events.ToArray())
            WriteProgress(100)

            Console.WriteLine("")

            dateFinishTimeIPScan = DateTime.Now()
            Console.WriteLine("")
            Console.WriteLine(DateTime.Now.ToString() & " - Scanning of SMB Shares finished")


            Dim Totalseconds As Long = DateDiff(DateInterval.Second, dateStartTimeIPScan, dateFinishTimeIPScan)

            Console.WriteLine("Took a total of " & Totalseconds & " seconds")

        Else
            If blnSingleHostToScan = True Then
                If strIPToScan <> "" Then
                    dictIPs.Clear()
                    dictIPs.Add(strIPToScan, 0)
                    Dim abc As SMBScanThreadObj
                    abc = New SMBScanThreadObj
                    abc.ObjectID = 1
                    abc.signal = New ManualResetEvent(False)
                    abc.ip = strIPToScan
                    dateStartTimeIPScan = DateTime.Now()
                    Console.WriteLine("")
                    Console.WriteLine(DateTime.Now.ToString() & " - SMB Share scan of " & strIPToScan & " begins")
                    Console.WriteLine("")


                    ' HMM .. what was I thinking 
                    'strPortList = Replace(strPortList, strDefaultPortsToScan & ",", "")
                    'strPortList = Replace(strPortList, strDefaultPortsToScan, "")


                    SMBScanCaller(abc)
                    dateFinishTimeIPScan = DateTime.Now()
                    Console.WriteLine("")
                    Console.WriteLine(DateTime.Now.ToString() & " - SMB Share scan of " & strIPToScan & " finished")

                End If
            End If

        End If














        'Console.ReadKey()

        ' Forces Visual Studio to wait on my keypress, otherwise is native won't fire
        If IsRunningInIDE() = True Then
            Console.WriteLine("")
            Console.WriteLine("Press any key to close")
            Console.ReadKey()
        End If



    End Sub
    Sub DisplayHelp()

        Console.WriteLine("Share Enumerator - Enumerates shares, written by Andrew Lear")
        Console.WriteLine("Using Win32 API calls, since NetApps don't do WMI ... ")
        Console.WriteLine("")
        Console.WriteLine("To scan a single machine the syntax is Samba_Enum.exe FQDN/IP  [/SHOWADMINSHARES]")
        Console.WriteLine("")
        Console.WriteLine("To scan multiple machines the synatax is Samba_Enum.exe TXT_File_List.txt [/SHOWADMINSHARES]")
        Console.WriteLine("")
        Console.WriteLine("/SHOWADMINSHARES is optional")

        Environment.Exit(1)

    End Sub
    Sub ReadTextFileOfAddresses(ByVal strTextFile As String)

        ' Reads in the text file .. checks each line is a IPv4 address or FQDN .. adds to appropiate dictionary

        Dim aryTmp() As String
        aryTmp = System.IO.File.ReadAllLines(strTextFile)

        Dim strLine As String

        dictIPs.Clear()
        dictFQDNs.Clear()
        dictIPsFromFile.Clear()

        Dim i As Integer
        For i = LBound(aryTmp) To UBound(aryTmp)
            strLine = Trim(aryTmp(i))
            If strLine <> "" Then
                If isIPv4(strLine) = True Then

                    If dictIPs.ContainsKey(strLine) = False Then
                        dictIPs.Add(strLine, 0) ' 
                    End If

                    If dictIPsFromFile.ContainsKey(strLine) = False Then
                        dictIPsFromFile.Add(strLine, 0) ' 
                    End If
                Else
                    If isFQDN(strLine) = True Then

                        If dictFQDNs.ContainsKey(strLine) = False Then
                            dictFQDNs.Add(strLine, "")
                        End If
                    End If

                End If
            End If
        Next

        'Console.WriteLine(dictIPs.Count & " IPv4 items")
        'Console.WriteLine(dictFQDNs.Count & " FQDN items")


    End Sub

    Sub ProcessCommandLineSwitches()

        Dim arguments As String() = Environment.GetCommandLineArgs()
        If UBound(arguments) = 0 Then
            DisplayHelp()

            Environment.Exit(79)
            ' No command line arguements
        Else
            For i = LBound(arguments) To UBound(arguments)

                If arguments(i).ToUpper().Contains("/?") Or arguments(i).ToUpper().Contains("-?") Then
                    DisplayHelp()
                    ' OK .. displayhelp
                    Environment.Exit(79)
                End If

                If File.Exists(arguments(i)) = True Then
                    strInputFile = arguments(i)
                    If Right(UCase(strInputFile), 4) <> ".EXE" Then
                        blnSingleHostToScan = False
                        ReadTextFileOfAddresses(strInputFile)
                    Else
                        strInputFile = ""
                    End If


                End If

                If arguments(i).ToUpper().Contains("/T:") Then
                    'blnOversideThreadCount = True
                    Dim strMaxThreads As String
                    strMaxThreads = arguments(i).Substring(arguments(i).IndexOf(":") + 1)
                    strMaxThreads = strMaxThreads.Trim()
                    If IsNumeric(strMaxThreads) Then
                        Try
                            intMaxThreads = CType(strMaxThreads, Integer)
                        Catch ex As Exception
                            intMaxThreads = 1
                        End Try

                        If intMaxThreads < 1 Then intMaxThreads = 1
                        If intMaxThreads > 64 Then intMaxThreads = 64

                    End If

                End If

                If isIPv4(arguments(i).ToUpper.Trim()) = True Then
                    strServer = arguments(i).ToUpper.Trim()
                    blnSingleHostToScan = True
                    strIPToScan = strServer
                End If

                If isFQDN(arguments(i).ToUpper.Trim()) = True Then
                    strServer = arguments(i).ToUpper.Trim()
                    blnSingleHostToScan = True
                    strFQDNToScan = strServer
                End If

                If arguments(i).ToUpper().Contains("/SHOWADMINSHARES") Then
                    blnShowAdminShares = True
                End If

            Next

        End If

        If strServer = "" And (dictIPs.Count = 0 And dictFQDNs.Count = 0 And dictIPsFromFile.Count = 0) Then
            Console.WriteLine("ERROR - No Input File, IPv4 or FQDN was found to enumerate within the command line args")
            Environment.Exit(99)
        End If


    End Sub

    Sub CreateMakeMasterCMDIfMissing()

        ' Creates the _Make_MASTER.CMD if missing

        Dim strBatch As String
        strBatch = ""

        strBatch = strBatch & "@echo off" & Environment.NewLine
        strBatch = strBatch & "@cls" & Environment.NewLine
        strBatch = strBatch & "@echo Please wait .. grab a coffee" & Environment.NewLine
        strBatch = strBatch & "@echo This MIGHT take a few minutes .. depends on how many files to merge" & Environment.NewLine
        strBatch = strBatch & "@echo." & Environment.NewLine
        strBatch = strBatch & "@echo Time started" & Environment.NewLine
        strBatch = strBatch & "@time /T" & Environment.NewLine
        strBatch = strBatch & "::@setLocal EnableDELAYedExpansion" & Environment.NewLine
        strBatch = strBatch & "if exist _MASTER.CSV del _MASTER.CSV" & Environment.NewLine
        strBatch = strBatch & "if exist Samba_Shares_Scan.CSV del Samba_Shares_Scan.CSV" & Environment.NewLine
        strBatch = strBatch & ":: This gets the name of the file CSV file in the folder" & Environment.NewLine
        strBatch = strBatch & "For /F " & Chr(34) & "Tokens=*" & Chr(34) & " %%I In ('dir *.csv /a-d /b /ON') do (" & Environment.NewLine
        strBatch = strBatch & "Set _FName=%%I" & Environment.NewLine
        strBatch = strBatch & "Goto GotFirstFile" & Environment.NewLine
        strBatch = strBatch & ")" & Environment.NewLine
        strBatch = strBatch & "" & Environment.NewLine
        strBatch = strBatch & ":GotFirstFile" & Environment.NewLine
        strBatch = strBatch & ":: Now reader the header row" & Environment.NewLine
        strBatch = strBatch & "for /f " & Chr(34) & "tokens=* delims= " & Chr(34) & " %%h in (%_FName%) do (" & Environment.NewLine
        strBatch = strBatch & "echo %%h > _MASTER.TXT" & Environment.NewLine
        strBatch = strBatch & "Goto GotHeaderRow" & Environment.NewLine
        strBatch = strBatch & ")" & Environment.NewLine
        strBatch = strBatch & "" & Environment.NewLine
        strBatch = strBatch & ":GotHeaderRow" & Environment.NewLine
        strBatch = strBatch & "" & Environment.NewLine
        strBatch = strBatch & ":: Now just enumerate the contents of each CSV file" & Environment.NewLine
        strBatch = strBatch & "for /f " & Chr(34) & "tokens=* delims= " & Chr(34) & " %%a in ('dir/b *.csv') do (" & Environment.NewLine
        strBatch = strBatch & "for /f " & Chr(34) & "skip=1 tokens=* delims= " & Chr(34) & " %%i in (%%a) do (" & Environment.NewLine
        strBatch = strBatch & "		echo %%i >> _MASTER.TXT" & Environment.NewLine
        strBatch = strBatch & "	)" & Environment.NewLine
        strBatch = strBatch & ")" & Environment.NewLine
        strBatch = strBatch & "" & Environment.NewLine
        strBatch = strBatch & "@del *.csv > NUL" & Environment.NewLine
        strBatch = strBatch & "@rename _MASTER.TXT Samba_Shares_Scan.CSV" & Environment.NewLine
        strBatch = strBatch & "@echo." & Environment.NewLine
        strBatch = strBatch & "@echo Time finished" & Environment.NewLine
        strBatch = strBatch & "@time /T" & Environment.NewLine
        strBatch = strBatch & "@echo." & Environment.NewLine

        Try
            Dim objWriter As New System.IO.StreamWriter(strOutputFolder & "\!Make_MASTER.cmd", False)
            objWriter.Write(strBatch)
            objWriter.Close()
        Catch ex As Exception
            Console.WriteLine("Failed to create output !Make_MASTER.CMD")
        End Try


    End Sub

    Private Sub SetupEnv()


        If Right(System.AppDomain.CurrentDomain.BaseDirectory(), 1) = "\" Then
            strOutputFolder = System.AppDomain.CurrentDomain.BaseDirectory() & "Results"
        Else
            strOutputFolder = System.AppDomain.CurrentDomain.BaseDirectory() & "\Results"
        End If

        If System.IO.Directory.Exists(strOutputFolder) = False Then
            Try
                System.IO.Directory.CreateDirectory(strOutputFolder)
            Catch ex As Exception
                Console.WriteLine("Failed to make '" & strOutputFolder & "', so quitting")
                Environment.Exit(100)
            End Try
        End If

        If System.IO.File.Exists(strOutputFolder & "\!Make_Master.cmd") = False Then
            Try
                CreateMakeMasterCMDIfMissing()
            Catch ex As Exception
                Console.WriteLine("Failed to create '" & strOutputFolder & "\!Make_MASTER.cmd' , not critical but not good")
            End Try
        End If

    End Sub
    Private Function WaitForAll(ByVal events As ManualResetEvent()) As Boolean
        Dim result As Boolean = False
        Try
            If events IsNot Nothing Then
                For i As Integer = 0 To events.Length - 1
                    events(i).WaitOne()
                Next
                result = True
            End If
        Catch
            result = False
        End Try
        Return result
    End Function

    Private Sub ResolveFQDNCaller(ByVal state As [Object])
        Dim obj As FQDNResolvingThreadObj = TryCast(state, FQDNResolvingThreadObj)
        If obj.fqdn Is Nothing Then
            obj.signal.[Set]()
            RegisterFQDNResolveThread()
            Return
        End If
        Dim FQDN As [String] = obj.fqdn

        Dim strResolvedIP As String = ""

        strResolvedIP = ResolveFQDN_to_IPv4(obj.fqdn)

        If strResolvedIP <> "" Then

            If dictIPs_to_FQDN.ContainsKey(strResolvedIP) = False Then
                SyncLock threads2Lock
                    dictIPs_to_FQDN.Add(strResolvedIP, FQDN)
                End SyncLock
            End If

            ' Do something, e.g add to stack if not in already
            If dictIPs.ContainsKey(strResolvedIP) = False Then
                SyncLock threads2Lock
                    dictIPs.Add(strResolvedIP, 0) ' Only add to stack when you are sure nothing else is using it
                End SyncLock
            End If

        End If

        RegisterFQDNResolveThread()
        obj.signal.[Set]()

    End Sub

    Public Sub RegisterFQDNResolveThread()
        SyncLock threads2Lock
            threadsStartedFQDN += 1
        End SyncLock
    End Sub

    Public Function GetNumberOfFQDNThreadsStarted() As Integer
        SyncLock threads2Lock
            Return threadsStartedFQDN
        End SyncLock
    End Function

    Public events As New List(Of ManualResetEvent)()

    Private Function HasFinished(ByVal events As ManualResetEvent()) As Boolean
        If events IsNot Nothing Then
            For i As Integer = 0 To events.Length - 1
                Dim success As Boolean = events(i).WaitOne(0)
                If Not success Then
                    Return False
                End If
            Next
        End If
        Return True
    End Function

    Public Sub WriteProgress(ByVal percentage As Integer)

        Dim width As Integer = Console.WindowWidth
        width = width - 9

        Dim working As Double = 0.0
        working = width / 100.0 * percentage
        Dim progressWidth As Integer = CInt(working)
        Console.Write(vbCr & "[")
        For i As Integer = 1 To progressWidth
            Console.Write("#")
        Next
        Console.SetCursorPosition(width - 5, Console.CursorTop)
        Console.Write("] " + CStr(percentage) + "%")



    End Sub


    Sub SetNumberOfThreadsBasedUponProcessors()

        Dim intNumberOfProcsToUse As Integer
        intNumberOfProcsToUse = 0

        Dim intNumCores As Integer

        Dim c As Integer

        On Error Resume Next ' just in case any of this falls over .. besides 'NumberOfCores' is only present on Windows 2008 R2 WMI 

        Dim query As System.Management.ManagementObjectSearcher = New System.Management.ManagementObjectSearcher("SELECT * From Win32_Processor ")
        Dim queryCollection As System.Management.ManagementObjectCollection = query.Get()

        For Each item In queryCollection
            intNumCores = 1
            c = 0
            c = CInt(item("NumberOfCores").ToString)

            If c > 0 Then
                intNumCores = c
            End If

            intNumberOfProcsToUse = intNumberOfProcsToUse + intNumCores

        Next


        On Error GoTo 0

        If intNumberOfProcsToUse = 0 Then
            intNumberOfProcsToUse = 1
        End If

        intMaxThreads = intNumberOfProcsToUse


    End Sub

    Private Sub SMBScanCaller(ByVal state As [Object])

        Dim obj As SMBScanThreadObj = TryCast(state, SMBScanThreadObj)


        Dim strIP As String = ""
        Dim strFQDN As String = ""
        Dim strHostname As String = ""
        Dim strDomainName As String = ""

        Dim strMACAddress As String = ""
        Dim strMACVendor As String = ""

        Dim strOS As String = ""
        Dim strServer As String = ""

        strIP = obj.ip

        If dictIPs_to_FQDN.ContainsKey(strIP) = True Then
            strFQDN = Trim(UCase(dictIPs_to_FQDN(strIP)))

        Else
            ' OK .. try a reverse DNS lookup
            strFQDN = UCase(Reverse_DNS_Lookup(strIP))
        End If

        strFQDN = UCase(strFQDN)

        Dim NetBIOS_data As NBNSData
        NetBIOS_data = SendNBNSPacket(strIP, 137)

        If NetBIOS_data.NetbiosComputer <> "" Then
            strHostname = Trim(UCase(NetBIOS_data.NetbiosComputer))
        End If

        If NetBIOS_data.NetbiosGroup <> "" Then
            strDomainName = Trim(UCase(NetBIOS_data.NetbiosGroup))
        End If


        Dim strMACOUI As String = ""
        If NetBIOS_data.MacAddress <> "" Then
            strMACAddress = NetBIOS_data.MacAddress


            strMACOUI = strMACAddress.Substring(0, 8)

            If MACAddressDictionary.ContainsKey(strMACOUI) = True Then
                strMACVendor = MACAddressDictionary(strMACOUI)
            End If

        End If


        Dim SMB_Data As SMB_Info
        SMB_Data = Nothing
        If CanConnectToTCPPort(strIP, 445) = True Then
            SMB_Data = Get_SMB_OS_Info(strIP)
        End If


        If strDomainName = "" Then
            ' OK .. NetBIOS query has failed to get what we need
            If SMB_Data.SMB_Domain <> "" Then
                strDomainName = SMB_Data.SMB_Domain
            End If
        End If

        If strHostname = "" And strFQDN <> "" Then
            ' Guess somewhat at the hostname, based upon the FQDN
            strHostname = Trim(UCase(Trim(Left(strFQDN, InStr(strFQDN, ".") - 1))))

        End If

        If strMACVendor.ToUpper() = "NETAPP" Then
            strOS = "NetApp"
        Else
            strOS = SMB_Data.OS_Description_1
            If strOS = "" Then
                strOS = SMB_Data.OS_Description_2
            End If
        End If

        ' FIX for NetApp ... seems some of their MACs don't appear registered at https://standards.ieee.org/develop/regauth/oui/public.html
        If (strMACOUI = "02-15-17" Or strMACOUI = "02-A0-98") Then
            strOS = "NetApp"
            strMACVendor = "NetApp"
        End If

        If strMACVendor = "" Then
            strMACVendor = "Unknown"
        End If


        If strHostname = "" Then
            ' OK .. last ditch attempt here

            ' We have been unable to determine the Hostname via NetBIOS ... we don't know the FQDN so use the IP
            strHostname = strIP

        End If

        Dim strLineOfOutput As String = ""

        Dim strCSVHeader As String = ""

        Dim aryCSVHeader(13)
        Dim aryDataRow(UBound(aryCSVHeader))

        aryCSVHeader(0) = "Server"
        aryCSVHeader(1) = "Share"
        aryCSVHeader(2) = "Description"
        aryCSVHeader(3) = "Path"
        aryCSVHeader(4) = "CurrentConnections"
        aryCSVHeader(5) = "ScanDate"
        aryCSVHeader(6) = "FullSharePath"
        aryCSVHeader(7) = "FQDN"
        aryCSVHeader(8) = "IP"
        aryCSVHeader(9) = "NetBIOSDomain"
        aryCSVHeader(10) = "ScannedBy"
        aryCSVHeader(11) = "ScannedFrom"
        aryCSVHeader(12) = "MAC Vendor"
        aryCSVHeader(13) = "OS"

        Dim strScannedBy As String = ""
        If Environment.UserDomainName <> "" Then
            strScannedBy = Environment.UserDomainName
        End If

        Dim strCompName As String = ""
        If Environment.MachineName <> "" Then
            strCompName = Environment.MachineName
        End If


        Dim i As Integer
        For i = LBound(aryCSVHeader) To UBound(aryCSVHeader)
            strCSVHeader = strCSVHeader & Chr(34) & aryCSVHeader(i) & Chr(34) & ","
        Next

        strCSVHeader = Left(strCSVHeader, Len(strCSVHeader) - 1)
        strCSVHeader = strCSVHeader & Environment.NewLine

        Dim strOutput As String = ""
        strOutput = strOutput & strCSVHeader


        ' Could be interesting to look at http://www.vbforums.com/showthread.php?616267-Creating-shared-folders-and-specifying-share-permissions

        Dim shi As Trinet.Networking.ShareCollection = Nothing

        Dim intShareCount As Integer = 0

        Dim intNumberOfValidRowOutput As Integer
        intNumberOfValidRowOutput = 0

        Dim strFullSharePath As String = ""
        Dim strShare As String = ""
        Dim strShareComment As String = ""
        Dim strPathOnFS As String = ""
        Dim strCurrentConnections As String = ""

        Dim strScanDate As String = ""
        strScanDate = DateTime.Now.ToString("yyyyMMdd")



        Dim strDataRow As String = ""

        shi = Trinet.Networking.ShareCollection.GetShares(strIP)
        If shi IsNot Nothing Then
            '  int total = 0;
            Dim csvRows As New List(Of [String])()
            For Each si As Trinet.Networking.Share In shi

                strDataRow = ""

                intShareCount = intShareCount + 1

                'Console.WriteLine(si.ToString & " " & si.ShareType.ToString)

                strFullSharePath = Trim(si.ToString())

                If strFQDN <> "" Then

                    strFullSharePath = Replace(strFullSharePath, strIP, strFQDN)

                End If

                strShare = Trim(si.ToString())
                strShare = Replace(strShare, "\\" & strIP, "")    ' Seems the Share contains the Path
                strShare = Replace(strShare, "\", "")

                strShareComment = Trim(si.Remark)

                strPathOnFS = Trim(si.Path)
                strCurrentConnections = CStr(si.CurrentConnections)

                'Console.WriteLine(strShare)

                If blnShowAdminShares = True Or si.ShareType = Trinet.Networking.ShareType.Disk Then

                    If blnShowAdminShares = False And UCase(strShare) = "ADMIN$" Then
                        ' do nothing .. since on a NetApp it seems ADMINS$ is enumerated out
                    Else

                        ' `FQDN`,`IP`,`NetBIOSDomain`" & Environment.NewLine


                        aryDataRow(0) = strHostname
                        aryDataRow(1) = strShare
                        aryDataRow(2) = strShareComment
                        aryDataRow(3) = strPathOnFS
                        aryDataRow(4) = strCurrentConnections
                        aryDataRow(5) = strScanDate
                        aryDataRow(6) = strFullSharePath
                        aryDataRow(7) = strFQDN
                        aryDataRow(8) = strIP
                        aryDataRow(9) = strDomainName

                        aryDataRow(10) = strScannedBy
                        aryDataRow(11) = strCompName

                        aryDataRow(12) = strMACVendor
                        aryDataRow(13) = strOS

                        For i = LBound(aryDataRow) To UBound(aryDataRow)
                            strDataRow = strDataRow & Chr(34) & aryDataRow(i) & Chr(34) & ","
                        Next

                        strDataRow = Left(strDataRow, Len(strDataRow) - 1)
                        strDataRow = strDataRow & Environment.NewLine


                        'strLineOfOutput = "`" & strHostname & "`" & "," & "`" & strShare & "`" & "," & "`" & strShareComment & "`" & "," & "`" & strPathOnFS & "`" & "," & "`" & strCurrentConnections & "`" & "," & "`" & strScanDate & "`" & "," & "`" & strFullSharePath & "`" & "Environment.NewLine"
                        'strLineOfOutput = Replace(strLineOfOutput, "`", Chr(34))


                        strOutput = strOutput & strDataRow

                        intNumberOfValidRowOutput = intNumberOfValidRowOutput + 1

                    End If

                End If

                'Console.Write(strLineOfOutput)

                'csvRows.Add("""" + si.ToString().Replace(server, "").Replace("\\", "").Replace("\", "") + """,""" + si.Path + """,""" + si.CurrentConnections + """,""" + si.Remark + """")
            Next
        End If


        If intShareCount > 0 Then

            If intNumberOfValidRowOutput > 0 Then

        
                File.WriteAllText(strOutputFolder + "\" + strIP + ".csv", strOutput)
                'Console.WriteLine(strServer & " - Found " & CStr(intShareCount) & " shares")
            Else
                'Console.WriteLine(strServer & " - Found some shares, however they all seem Admin Shares, no output file created")
            End If
        End If


        'Console.WriteLine("")
        'Console.WriteLine(strOutput)
        'Console.WriteLine("")

        RegisterSMBScanningThread()

        obj.signal.[Set]()   ' Sets the signal that this thread is done

    End Sub

    Public Function GetNumberOfSMBScanThreadsStarted() As Integer
        SyncLock threads2Lock
            Return threadsStartedSMBScan
        End SyncLock
    End Function

    Public Sub RegisterSMBScanningThread()
        SyncLock threads2Lock
            threadsStartedSMBScan += 1
        End SyncLock
    End Sub

End Module
