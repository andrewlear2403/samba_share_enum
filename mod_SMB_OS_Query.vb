﻿Imports System
Imports System.Net
Imports System.Net.Sockets
Imports System.Text

Module mod_SMB_OS_Query

    Public Structure SMB_Info

        Public SMB_Domain As String
        Public OS_Description_1 As String
        Public OS_Description_2 As String
    End Structure

    Public Function Get_SMB_OS_Info(ByVal strIP As String) As SMB_Info

        Dim objResult As SMB_Info
        objResult = Nothing


        objResult.OS_Description_1 = ""
        objResult.OS_Description_2 = ""
        objResult.SMB_Domain = ""


        ' Connect to a remote device.
        Dim ipAddress As IPAddress
        ipAddress = System.Net.IPAddress.Parse(strIP)

        ' Bind to port 139
        Dim remoteEP As New IPEndPoint(IPAddress, 139)

        ' Create a TCP/IP socket.
        Dim sender As New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)


        Try
            Dim SMB_SessionRequest As Byte() = New Byte() {&H81, &H0, &H0, &H44, &H20, &H43, &H4B, &H46, &H44, &H45, &H4E, &H45, &H43, &H46, &H44, &H45, &H46, &H46, &H43, &H46, &H47, &H45, &H46, &H46, &H43, &H43, &H41, &H43, &H41, &H43, &H41, &H43, &H41, &H43, &H41, &H43, &H41, &H0, &H20, &H45, &H4B, &H45, &H44, &H46, &H45, &H45, &H49, &H45, &H44, &H43, &H41, &H43, &H41, &H43, &H41, &H43, &H41, &H43, &H41, &H43, &H41, &H43, &H41, &H43, &H41, &H43, &H41, &H43, &H41, &H41, &H41, &H0}
            Dim SMB_Negotiate As Byte() = New Byte() {&H0, &H0, &H0, &H2F, &HFF, &H53, &H4D, &H42, &H72, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H5C, &H2, &H0, &H0, &H0, &H0, &H0, &HC, &H0, &H2, &H4E, &H54, &H20, &H4C, &H4D, &H20, &H30, &H2E, &H31, &H32, &H0}
            Dim SMB_SetupAccount As Byte() = New Byte() {&H0, &H0, &H0, &H48, &HFF, &H53, &H4D, &H42, &H73, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H5C, &H2, &H0, &H0, &H0, &H0, &HD, &HFF, &H0, &H0, &H0, &HFF, &HFF, &H2, &H0, &H5C, &H2, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H1, &H0, &H0, &H0, &HB, &H0, &H0, &H0, &H4A, &H43, &H0, &H41, &H54, &H54, &H48, &H43, &H0}

            Dim bytes(256) As Byte

          
            sender.ReceiveTimeout = 1200
            sender.SendTimeout = 1200

            ' Connect the socket to the remote endpoint.
            sender.Connect(remoteEP)


            ' Encode the data string into a byte array.
            Dim msg As Byte()
            Dim bytesSent As Integer
            Dim bytesRec As Integer


            msg = SMB_SessionRequest
            bytesSent = sender.Send(msg)
            bytesRec = sender.Receive(bytes)

            If bytesRec = 0 Then
                'Console.WriteLine("error")
                Get_SMB_OS_Info = objResult
                Exit Function

            End If



            msg = SMB_Negotiate
            ' Send the data through the socket.

            bytesSent = sender.Send(msg)
            bytesRec = sender.Receive(bytes)

            If bytesRec = 0 Then
                Get_SMB_OS_Info = objResult
                Exit Function
            End If


            msg = SMB_SetupAccount
            ' Send the data through the socket.

            bytesSent = sender.Send(msg)
            bytesRec = sender.Receive(bytes)

            If bytesRec = 0 Then
                'Console.WriteLine("SMB query for OS failed")
                Get_SMB_OS_Info = objResult
                Exit Function

            ElseIf bytesRec > 0 Then

                Dim strAndrew As String
                strAndrew = ""

                Dim i
                i = bytesRec

                i = i - 1

                While (System.Threading.Interlocked.Decrement(i) > 0)

                    'Console.Write(Chr(bytes(i + 1)))
                    strAndrew = strAndrew + Chr(bytes(i + 1))

                End While

                If strAndrew <> "" Then

                    Dim aryTest()
                    aryTest = Split(strAndrew, Chr(0))

                    For j = LBound(aryTest) To UBound(aryTest)
                        aryTest(j) = Trim(StrReverse(aryTest(j)))
                    Next

                    Dim p
                    p = 0
                    For j = LBound(aryTest) To UBound(aryTest)

                        If Trim(aryTest(j)) <> "" Then
                            'Console.WriteLine(aryTest(j))
                        End If

                        p = p + 1
                        If p > 3 Then
                            Exit For
                        End If
                    Next

                    If UBound(aryTest) >= 2 Then

                        objResult.SMB_Domain = Trim(aryTest(1))
                        objResult.OS_Description_1 = Trim(aryTest(2))
                        objResult.OS_Description_2 = Trim(aryTest(3))


                        ' Clean up some of the values
                        objResult.OS_Description_1 = Replace(objResult.OS_Description_1, " 3790 ", " ")
                        objResult.OS_Description_1 = Replace(objResult.OS_Description_1, "5.1", "XP")
                        objResult.OS_Description_1 = Replace(objResult.OS_Description_1, "5.0", "2000")
                        objResult.OS_Description_1 = Replace(objResult.OS_Description_1, " 5.2", "")
                        objResult.OS_Description_1 = Replace(objResult.OS_Description_1, "Service Pack ", "SP")



                        objResult.OS_Description_2 = Replace(objResult.OS_Description_2, " 3790 ", " ")
                        objResult.OS_Description_2 = Replace(objResult.OS_Description_2, "5.1", "XP")
                        objResult.OS_Description_2 = Replace(objResult.OS_Description_2, "5.0", "2000")
                        objResult.OS_Description_2 = Replace(objResult.OS_Description_2, " 5.2", "")
                        objResult.OS_Description_2 = Replace(objResult.OS_Description_2, "Service Pack ", "SP")

                    End If

                End If

            End If

            ' Release the socket.
            sender.Shutdown(SocketShutdown.Both)
            sender.Close()
        Catch ex As Exception
            If sender.Connected = True Then
                sender.Shutdown(SocketShutdown.Both)
                sender.Close()

            End If

            objResult.OS_Description_1 = ""
            objResult.OS_Description_2 = ""
            objResult.SMB_Domain = ""
        End Try




        

        Get_SMB_OS_Info = objResult

    End Function


End Module
