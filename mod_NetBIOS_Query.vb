﻿Imports System.Collections.Generic
Imports System.Text
Imports System.Net
Imports System.Net.Sockets

Module mod_NetBIOS_Query
    Public Function NBQuery() As Byte()

        Dim bdata As Byte() = New Byte(49) {&H80, &HFF, &H0, &H0, &H0, &H1, _
         &H0, &H0, &H0, &H0, &H0, &H0, _
         &H20, &H43, &H4B, &H41, &H41, &H41, _
         &H41, &H41, &H41, &H41, &H41, &H41, _
         &H41, &H41, &H41, &H41, &H41, &H41, _
         &H41, &H41, &H41, &H41, &H41, &H41, _
         &H41, &H41, &H41, &H41, &H41, &H41, _
         &H41, &H41, &H41, &H0, &H0, &H21, _
         &H0, &H1}

        Return bdata
    End Function

    Public Function SendNBNSPacket(ByVal strIP As String, ByVal intPortNum As Integer) As NBNSData


        'NBNS Data 
        Dim nbnsData As New NBNSData()
        nbnsData.MacAddress = ""
        nbnsData.NetbiosComputer = ""
        nbnsData.NetbiosGroup = ""

        Try
            'IPAddress in 
            Dim server As IPAddress = IPAddress.Parse(strIP)




            If strIP = "0.0.0.0" Or isIPv4(strIP) = False Then
                SendNBNSPacket = nbnsData
                Exit Function
            End If


            'Declare MAC Address 
            Dim strMAC As String = ""
            Dim strNetbiosComputer As String = ""
            Dim strNetbiosGroup As String = ""
            Dim strNetbiosUser As String = ""

            Dim soc As New Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp)



            Dim ipEndPT As New System.Net.IPEndPoint(IPAddress.Parse(strIP), 137)
            Dim endPT As System.Net.EndPoint = DirectCast(ipEndPT, System.Net.EndPoint)

            Dim inIPEndPT As New System.Net.IPEndPoint(IPAddress.Parse(strIP), intPortNum)
            Dim inEndPT As System.Net.EndPoint = DirectCast(inIPEndPT, System.Net.EndPoint)

            soc.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendTimeout, intTimeout)
            soc.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, intTimeout)


            'Datagram declaration expected to receive from host 
            Dim dataIn As Byte() = New Byte(4095) {}


            Dim intTries As Integer = 0

            Dim bolResponded As Boolean = False

            Do
                soc.SendTo(NBQuery(), endPT)
                Try
                    'Get data from host 
                    soc.ReceiveFrom(dataIn, inEndPT)
                    bolResponded = True
                Catch se As SocketException
                    intTries += 1
                    'handleTimed out 
                    If se.ErrorCode = 10060 Then
                    End If
                End Try
            Loop While (Not bolResponded) AndAlso intTries < 1

            If bolResponded Then
                'parse the MAC Address from the data received from host 


                strMAC = getMAC(dataIn)
                strNetbiosComputer = getNebiosComputer(dataIn)
                strNetbiosGroup = getNebiosGroup(dataIn)
                strNetbiosUser = getRemoteMachineNameTable(dataIn).Count.ToString()

                nbnsData.MacAddress = strMAC
                nbnsData.NetbiosComputer = strNetbiosComputer
                nbnsData.NetbiosGroup = strNetbiosGroup


            Else
                ' Add any defaults values you want here 
            End If


        Catch ex As Exception

        End Try

        SendNBNSPacket = NBNSData
       

    End Function

    Public Class NBNSData
        Public Property MacAddress() As String
            Get
                Return m_MacAddress
            End Get
            Set(ByVal value As String)
                m_MacAddress = Value
            End Set
        End Property
        Private m_MacAddress As String
        Public Property NetbiosUser() As String
            Get
                Return m_NetbiosUser
            End Get
            Set(ByVal value As String)
                m_NetbiosUser = Value
            End Set
        End Property
        Private m_NetbiosUser As String
        Public Property NetbiosComputer() As String
            Get
                Return m_NetbiosComputer
            End Get
            Set(ByVal value As String)
                m_NetbiosComputer = Value
            End Set
        End Property
        Private m_NetbiosComputer As String
        Public Property NetbiosGroup() As String
            Get
                Return m_NetbiosGroup
            End Get
            Set(ByVal value As String)
                m_NetbiosGroup = Value
            End Set
        End Property
        Private m_NetbiosGroup As String
    End Class

    Public Class NBNS

        'Recieves an IP address and any available port to List<String>en on 
        Public Function SendNBNSPacket(ByVal strIPCur As String, ByVal intPortNum As Integer) As NBNSData
            'IPAddress in 
            Dim server As IPAddress = IPAddress.Parse(strIPCur)

            'NBNS Data 
            Dim nbnsData As New NBNSData()

            'Declare MAC Address 
            Dim strMAC As String = ""
            Dim strNetbiosComputer As String = ""
            Dim strNetbiosGroup As String = ""
            Dim strNetbiosUser As String = ""

            Dim soc As New Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp)

            Dim ipEndPT As New System.Net.IPEndPoint(IPAddress.Parse(strIPCur), 137)
            Dim endPT As System.Net.EndPoint = DirectCast(ipEndPT, System.Net.EndPoint)

            Dim inIPEndPT As New System.Net.IPEndPoint(IPAddress.Parse(strIPCur), intPortNum)
            Dim inEndPT As System.Net.EndPoint = DirectCast(inIPEndPT, System.Net.EndPoint)

            soc.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendTimeout, 50)
            soc.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 50)

            'Datagram declaration expected to receive from host 
            Dim dataIn As Byte() = New Byte(4095) {}


            Dim intTries As Integer = 0

            Dim bolResponded As Boolean = False

            Do
                soc.SendTo(NBQuery(), endPT)
                Try
                    'Get data from host 
                    soc.ReceiveFrom(dataIn, inEndPT)
                    bolResponded = True
                Catch se As SocketException
                    intTries += 1
                    'handleTimed out 
                    If se.ErrorCode = 10060 Then
                    End If
                End Try
            Loop While (Not bolResponded) AndAlso intTries < 1

            If bolResponded Then
                'parse the MAC Address from the data received from host 
                strMAC = getMAC(dataIn)
                strNetbiosComputer = getNebiosComputer(dataIn)
                strNetbiosGroup = getNebiosGroup(dataIn)
                strNetbiosUser = getRemoteMachineNameTable(dataIn).Count.ToString()
            Else
                strMAC = "00-00-00-00-00-00"
            End If

            nbnsData.MacAddress = strMAC
            nbnsData.NetbiosComputer = strNetbiosComputer
            nbnsData.NetbiosGroup = strNetbiosGroup
            nbnsData.NetbiosUser = strNetbiosUser

            Return nbnsData
        End Function

        'NBNS Request Packet Contents 
        


        'Parse the Packet sent by the host and get the value @byt 56 
        

    End Class

    Private Function getMAC(ByVal dtResponse As Byte()) As String
        Dim strMac As String = ""
        'intStart reads the value @ byte 56 which contains the 
        'no of names in the Response Packet 
        Dim intStart As Integer = CInt(dtResponse(56))

        '1 name contains 16 bytes followed by 2 bytes name flag = 18 Bytes 
        Dim intOffset As Integer = 56 + intStart * 18 + 1

        For x As Integer = 0 To 5
            strMac += [String].Format("{0:X2}", dtResponse(intOffset + x)) + "-"
        Next

        strMac = strMac.Remove(strMac.Length - 1, 1)
        Return strMac
    End Function

    Private Function getNebiosComputer(ByVal dtResponse As Byte()) As String
        ' string strNetbiosComputer = ""; 
        '
        '    int intStart = (int)dtResponse[56]; 
        '
        '    int intOffset = 57; 
        '    for (int x = 0; x < 15; x++) 
        '    { 
        '    strNetbiosComputer += Convert.ToChar(dtResponse[intOffset + x]); 
        '    } 
        '    return strNetbiosComputer; 
        '    
        '    

        Dim netbiosComputer As [String] = ""
        Dim better As List(Of [String]) = getRemoteMachineNameTable(dtResponse)
        For Each tmpString As [String] In better
            If tmpString.Substring(15, 2) = "00" AndAlso tmpString.Substring(17, 1) = "0" Then
                netbiosComputer = Trim(tmpString.Substring(0, 15))

                netbiosComputer = Replace(netbiosComputer, Chr(32), "")

                If netbiosComputer.Contains("~") = True Or netbiosComputer.Contains(" ") = True Then
                    Continue For
                Else
                    'Debug.Print(Asc(tmpString.Substring(3, 1)))
                    Exit For
                End If


            End If
        Next

        netbiosComputer = UCase(Trim(netbiosComputer))
        Return netbiosComputer

    End Function

    Private Function getRemoteMachineNameTable(ByVal dtResponse As Byte()) As List(Of [String])
        Dim strNoOfRemoteMachineName As Int32 = 0
        Dim CurrentName As [String] = ""
        Dim RemoteMachineNames As New List(Of [String])()

        'Get no of names in the Response Packet (position 56 of response Packet) 
        strNoOfRemoteMachineName = Convert.ToInt32(dtResponse(56))

        For remoteNameCounter As Integer = 0 To strNoOfRemoteMachineName - 1
            CurrentName = ""
            'Get first 15 characters after position 56 
            For x As Integer = 1 To 15
                '18 characters per name, 
                Dim newByte As Byte = dtResponse(x + (56 + remoteNameCounter * 18))

                'checked if printable chars 0x21 to 0x7E 
                '0x20 = space 
                '0x7F = Delete 
                'TRAP GROUP NAMES - Must have a 1E NetBios Suffix 
                If newByte > &H19 AndAlso newByte < &H7E Then
                    CurrentName += Convert.ToChar(newByte)
                Else
                    CurrentName += " "
                End If
            Next

            'HEC Notes 
            'NAME_FLAGS = Bytes 17 and 18 Definition 
            'Taken from http://www.faqs.org/rfcs/rfc1002.html 
            ' 
            'RESERVED 7-15 Reserved for future use. Must be zero (0). 
            'PRM 6 Permanent Name Flag. If one (1) then entry 
            ' is for the permanent node name. Flag is zero 
            ' (0) for all other names. 
            'ACT 5 Active Name Flag. All entries have this flag 
            ' set to one (1). 
            'CNF 4 Conflict Flag. If one (1) then name on this 
            ' node is in conflict. 
            'DRG 3 Deregister Flag. If one (1) then this name 
            ' is in the process of being deleted. 
            'ONT 1,2 Owner Node Type: 
            ' 00 = B node 
            ' 01 = P node 
            ' 10 = M node 
            ' 11 = Reserved for future use 
            'G 0 Group Name Flag. 
            ' If one (1) then the name is a GROUP NetBIOS 
            ' name. 
            ' If zero (0) then it is a UNIQUE NetBIOS name. 

            RemoteMachineNames.Add(CurrentName + [String].Format("{0:X2}", dtResponse(16 + (56 + remoteNameCounter * 18))) + Convert.ToString(dtResponse(17 + (56 + remoteNameCounter * 18)), 2).PadLeft(8, "0"c))
        Next

        Return RemoteMachineNames
    End Function

    Private Function getNebiosGroup(ByVal dtResponse As Byte()) As String
        Dim netbiosGroupName As [String] = ""
        For Each tmpString As String In getRemoteMachineNameTable(dtResponse)


            If tmpString.Substring(15, 2) = "1E" Or tmpString.Substring(15, 4) = "0010" Then
                netbiosGroupName = Trim(tmpString.Substring(0, 15))
                netbiosGroupName = Replace(netbiosGroupName, Chr(32), "")
                'Exit For

                If netbiosGroupName.Contains("~") Or netbiosGroupName.Contains(" ") Or netbiosGroupName.Contains("_") Then
                    Continue For
                End If
                Exit For

            End If
        Next

        If netbiosGroupName = "" Then

            ' OK .. didn't get anything using normal routines .. so use this
            For Each tmpString As String In getRemoteMachineNameTable(dtResponse)

                If tmpString.Substring(15, 4) = "0011" Then
                    netbiosGroupName = Trim(tmpString.Substring(0, 15))
                    'Exit For

                    If netbiosGroupName.Contains("~") Or netbiosGroupName.Contains(" ") Or netbiosGroupName.Contains("_") Then
                        Continue For
                    End If
                    Exit For

                End If
            Next


        End If

        netbiosGroupName = Trim(netbiosGroupName)

        Return netbiosGroupName
    End Function



End Module
